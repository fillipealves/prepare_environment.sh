#!/bin/bash
#set -x 
# Substitua o '?' pelo nome do cliente que deseja aplicar as customizações
# Ex: descomplica
#Verifique se o script está se referindo aos diretórios atualizados do GITHUB

CLIENTE=exemplo
# Apagar pasta do git antiga
sudo rm -rf /home/ubuntu/edu-lyc-customizacoes-clientes

# Baixar as customizações do cliente
sudo git clone https://github.com/technecloud/edu-lyc-customizacoes-clientes.git --depth 1

# Copiando arquivos do diretório baixado para o responsivo da aplicação
sudo cp -R /home/ubuntu/edu-lyc-customizacoes-clientes/${CLIENTE}/AOnline3/*  /opt/techne/lyceum/tomcat/webapps/AOnline3/

# Obtendo nome do arquivo custom.css antigo e atribuindo a variável $css_name
css_name=`ls /opt/techne/lyceum/tomcat/webapps/AOnline3/_css/custom-*.css | awk -F '-' {'print $2'} | awk -F '.css' {'print $1'}`
# ls -1 | awk -F "-" '{print $2}' | cut -d "." -f1


# Substituindo o arquivo antigo pelo novo
sudo cp /opt/techne/lyceum/tomcat/webapps/AOnline3/css/custom.css /opt/techne/lyceum/tomcat/webapps/AOnline3/_css/custom-${css_name}.css

# Apagar pasta do git criada
sudo rm -rf /home/ubuntu/edu-lyc-customizacoes-clientes