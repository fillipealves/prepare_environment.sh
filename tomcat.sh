#!/bin/bash
qtde=$(ps aux | grep "java" | wc -l)

if test "$qtde" -lt "2"
then
echo "tomcat is offline";
echo "Starting...";
systemctl start tomcat
else
echo "tomcat is online." ;
echo "Nothing to do.";
fi

