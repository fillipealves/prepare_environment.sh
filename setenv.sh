SYSTEM_RESERVED_MEMORY=1024

if [ -z "$MEM_TOTAL_KB" ]; then
    MEM_TOTAL_KB=$(cat /proc/meminfo | grep MemTotal | awk '{print $2}')
fi
MEM_JAVA_MB=$(($MEM_TOTAL_KB / 1024 - $SYSTEM_RESERVED_MEMORY))

JAVA_OPTS="-Djava.awt.headless=true \
-Xmx${MEM_JAVA_MB}m \
-XX:+UseConcMarkSweepGC \
-Dfile.encoding=UTF8 \
-Duser.timezone=America/Sao_Paulo \
-Djava.security.auth.login.config=/etc/techne/lyceum/jaas.config \
-Dlog4j.configurationFile=/opt/techne/lyceum/tomcat/conf/log4j2.xml \
-Dcronos.xsrf.sop=false"
CATALINA_PID="/tmp/lyceum.pid"
JAVA_HOME="/usr/lib/jvm/java-8-oracle"
JRE_HOME="/usr/lib/jvm/java-8-oracle/jre"
LANG="pt_BR.UTF-8"
CATALINA_OPTS="-Dcom.sun.management.jmxremote \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.port=8989 \
$CATALINA_OPTS"
