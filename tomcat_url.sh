#!/bin/bash
###########################################################################
## pmenos_check_fidelizacao.sh - Checa processo criptografia tomcat      ##
## Escrito por: Bruno Cavalcanti (Fortaleza - Ceara)                     ##
## E-mail: brunocavalcanti@pmenos.com.br                                 ##
## Servidor: tomcat ng                                                   ##
###########################################################################

path_script="/root"
path_tomcat="/opt/techne/lyceum/tomcat"
http_proxy=
wget -T 5 --tries=3 --spider --no-proxy http://localhost:8080/secretaria/style/rainyblue/page.css 2> $path_script/check_tomcat_url.log
RESPONSE=`cat $path_script/check_tomcat_url.log | grep response | cut -d " " -f 6`
PROCESSO="tomcat"
echo "${RESPONSE}"


if test ${RESPONSE} -eq 200; then
        echo "${PROCESSO} executando normalmente... `date`" >> /var/log/tomcat_url_status
else
        echo "${PROCESSO} nÃ£o esta executando normalmente... `date`" >> /var/log/tomcat_url_status
        echo "Pid atual: $(ps faux | grep java | grep -v grep | awk '{print $2}')" >> /var/log/tomcat_url_status
        echo "Parando ${PROCESSO}..." >> /var/log/tomcat_url_status
        kill -9 $(ps faux | grep java | grep -v grep | awk '{print $2}') >> /var/log/tomcat_url_status
        sleep 3
        echo "Reiniciando ${PROCESSO}..." >> /var/log/tomcat_url_status
        $path_tomcat/bin/startup.sh >> /var/log/ctomcat_url_status
fi

HORA="$(date | cut -d " " -f 4 | cut -d ":" -f 1)"

if [ $HORA -eq "23" ]; then
        :>/var/log/tomcat_url_status
fi

wget --no-check-certificate --delete-after --verbose --method POST --timeout=0 --header '' 'http://10.225.32.17:8080/secretaria/login?username=zeus&password=bhimA123' 2> $path_script/saida.check

STATUS_AUTH=$(cat $path_script/saida.check | grep "response" | tail -n 1 | awk -F "response..." '{print $2}' | awk '{print $1}')

if test "$STATUS_AUTH" == "200"; then
        echo "Autenticação Ok"
else
        echo "Autenticação com erro" >> /var/log/tomcat_url_status
                echo "${PROCESSO} nÃ£o esta executando normalmente... `date`" >> /var/log/tomcat_url_status
        echo "Pid atual: $(ps faux | grep java | grep -v grep | awk '{print $2}')" >> /var/log/tomcat_url_status
        echo "Parando ${PROCESSO}..." >> /var/log/tomcat_url_status
        kill -9 $(ps faux | grep java | grep -v grep | awk '{print $2}') >> /var/log/tomcat_url_status
        sleep 3
        echo "Reiniciando ${PROCESSO}..." >> /var/log/tomcat_url_status
        $path_tomcat/bin/startup.sh >> /var/log/ctomcat_url_status
fi

