#!/bin/bash
######################################################
# Script Preparar Ambiente                           #
# Data 07/02/2020                                    #
# Sistema Operacional, Ubuntu Server                 #
######################################################

# Função para criar Script para reinicialização do tomcat
createTomcatScript(){
  echo "Criando Script para reinicialização do tomcat em: /root/tomcat.sh"
  touch /root/tomcat.sh
  cat ./tomcat.sh > /root/tomcat.sh
  sh -c 'chmod +x /root/tomcat.sh'
  clear
}

# Função para criar Script para teste de url
createURLscript(){
  echo "Criando Script para teste de URL em: /root/tomcat.sh"
  touch /root/tomcat_url.sh
  cat ./tomcat_url.sh > /root/tomcat_url.sh
  sh -c 'chmod +x /root/tomcat_url.sh'
  clear
}

#Função para adicionar linha no crontab
schedulingCrontab(){
  echo "Adicionando comando para rodar o tomcat.sh no Crontab"
  cat ./crontab > /var/spool/cron/crontabs/root
  clear
}

# Função de instalação do Oracle Java
installingJava(){
  echo "Deletando openjdk default"
  apt purge default-jdk -y 2> /dev/null
  clear
  echo "Instalando a versão 8 do java"
  echo "Descompactando artefato"
  echo "Baixando artefato do Java8" 
  git clone https://fillipealves@bitbucket.org/fillipealves/java8.git
  tar -xzf ./java8/jdk-8u241-linux-x64.tar.gz
  clear
  echo "Criando diretorio jvm em: /usr/lib/jvm"
  mkdir /usr/lib/jvm
  clear
  echo "Movendo artefato para lib: em /usr/lib/jvm/java-8-oracle"
  mv jdk1.8.0_241 /usr/lib/jvm/java-8-oracle
}

# Função para criar Jaas.config
createJaasConfig(){
  echo "Jaas.Config adicionado"
  touch /opt/techne/lyceum/jaas.config
  cat ./jaas.config > /opt/techne/lyceum/jaas.config
}

# Função de instalação do tomcat
installingTomcat(){
    # Criando usuario lyceum e grupo techne
    echo "Criando diretorio lyceum em: /opt/techne/lyceum"
    mkdir -p /opt/techne/lyceum
    echo "Criando usuário: lyceum"
    useradd -r -m -U -d /opt/techne/lyceum -s /bin/false lyceum
    clear
    echo "Criando grupo: techne"
    groupadd techne
    clear
  # Iniciando instalação do tomcat 7.0.72
  echo "Deletando artefatos do tomcat na tmp"
  rm -rf /tmp/apache-tomcat-7*
  clear
  echo "Fazendo download de arquivos do tomcat7"
  #wget https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.85/bin/apache-tomcat-7.0.85.tar.gz -P /tmp 2> /dev/null
  wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.43/bin/apache-tomcat-9.0.43.tar.gz -P /tmp 2> /dev/null
  clear
  echo "Descompactando arquivo baixado"
  #tar xf /tmp/apache-tomcat-7.0.85.tar.gz -C /opt/techne/lyceum
  tar xf /tmp/apache-tomcat-9.0.43.tar.gz -C /opt/techne/lyceum
  clear
  echo "Renomeando diretorio padrão /apache-tomcat-7 para /tomcat"
  mv /opt/techne/lyceum/apache-tomcat-9* /opt/techne/lyceum/tomcat
  clear
  echo "Dando permissão a usuario e grupo no diretório tomcat" 
  chown -R lyceum:techne /opt/techne
  chown lyceum:techne /opt
  clear
  echo "Substituindo arquivo tomcat.service"
  cat ./tomcat.service > /etc/systemd/system/tomcat.service
  clear
  echo "Substituindo arquivo setenv.sh"
  cat ./setenv.sh > /opt/techne/lyceum/tomcat/bin/setenv.sh
  clear
  echo "Dando permissões de execução nos scripts em: tomcat/bin"
  sh -c 'chmod +x /opt/techne/lyceum/tomcat/bin/*.sh'
  clear
  echo "Definindo variáveis de ambiente"
  echo "JAVA_HOME"
  export JAVA_HOME=/usr/lib/jvm/java-8-oracle
  echo "JRE_HOME"
  export JRE_HOME=/usr/lib/jvm/java-8-oracle/jre
  clear
  echo "Recarregando systemctl"
  systemctl daemon-reload
  echo "Iniciando tomcat"
  systemctl start tomcat
  systemctl enable tomcat
  clear
  echo "Deletando artefatos utilizados"
  rm -rf ./jdk1.8.0_241

# Deletando arquivos do repositório
# echo "Deletando arquivos baixados do repositório"
# rm -rf .git/ instala_tomcat.sh/

# Instalação concluída!
  echo "Tomcat instalado! Para verificar o status ou reiniciar o tomcat, execute os seguintes comandos:"
  echo " "
  echo "systemctl status tomcat"
  echo "systemctl restart tomcat"
  echo " "
  echo "Para parar o tomcat execute o seguinte comando"
  echo " "
  echo "systemctl stop tomcat"
}

# Chamando as funções criadas
#createCustomizationScript
createTomcatScript
#createURLscript
schedulingCrontab
installingJava
installingTomcat
createJaasConfig